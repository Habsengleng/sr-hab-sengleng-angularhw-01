import {
  AfterContentInit,
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { Week } from '../model/week';
import { ChangeColorDirective } from '../change-color.directive';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css'],
})
export class WeekRowComponent implements OnInit, OnChanges {
  @Input() week: Week;
  @Input() calPercent: any;
  changeColor = false;
  constructor() {}
  ngOnChanges(): void {
    // console.log(this.week.regular.used);
    if ((this.week.regular.used / this.week.regular.available) * 100 >= 70) {
      this.changeColor = false;
    } else {
      this.changeColor = true;
    }
  }
  ngOnInit(): void {}
}
