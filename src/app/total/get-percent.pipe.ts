import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getPercent'
})
export class GetPercentPipe implements PipeTransform {

  transform(min: number, max: number): number {
    return (min / max);
  }
}
