import {AfterContentInit, AfterViewInit, Component, DoCheck, Input, OnChanges, OnInit} from '@angular/core';
import {Status} from '../model/status';
import { Week } from '../model/week';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {
  // num: number;
  isSuccess = true;
  changeColor = {
    'bg-success': this.isSuccess,
    'bg-danger': !this.isSuccess
  };
  @Input() statuses: Week;
  @Input() calPercent: any;
  constructor() {  }

  ngOnInit(): void {

  }


}
