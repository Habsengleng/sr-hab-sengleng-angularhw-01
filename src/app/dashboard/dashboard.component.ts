import { Component, OnInit } from '@angular/core';
import { Status } from '../model/status';
import { Week } from '../model/week';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  num: number;
  constructor() {}

  ngOnInit(): void {}
  generateNumber(min, max): number {
    this.num = Math.floor(Math.random() * (max - min + 1) + min);
    return this.num;
  }
  generateTotal(): any {
    return {
      name: 'week',
      regular: {
        used: this.generateNumber(1, 30),
        available: 40,
      },
      diesel: {
        used: this.generateNumber(1, 20),
        available: 30,
      },
      premium: {
        used: this.generateNumber(1, 25),
        available: 25,
      },
    };
  }
  generateWeeks(): any {
    return {
      name: 'week',
      regular: {
        used: this.generateNumber(1, 30),
        available: 40,
      },
      diesel: {
        used: this.generateNumber(1, 20),
        available: 30,
      },
      premium: {
        used: this.generateNumber(1, 25),
        available: 25,
      },
    };
  }
  generateStatus(): any{
    return {
      name: 'week',
      regular: {
        used: this.generateNumber(1, 30),
        available: 40,
      },
      diesel: {
        used: this.generateNumber(1, 20),
        available: 30,
      },
      premium: {
        used: this.generateNumber(1, 25),
        available: 25,
      },
    };
  }
  calPercent(used, available): number {
    this.num = (used / available) * 100;
    return this.num;
  }
}
