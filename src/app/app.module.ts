import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TotalComponent } from './total/total.component';
import { WeekRowComponent } from './week-row/week-row.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BranchComponent } from './branch/branch.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GetPercentPipe } from './total/get-percent.pipe';
import { ChangeColorDirective } from './change-color.directive';

@NgModule({
  declarations: [
    AppComponent,
    TotalComponent,
    WeekRowComponent,
    DashboardComponent,
    BranchComponent,
    GetPercentPipe,
    ChangeColorDirective
  ],
  imports: [
    BrowserModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
