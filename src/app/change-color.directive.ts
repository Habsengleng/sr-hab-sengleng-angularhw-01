import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[appChangeColor]',
})
export class ChangeColorDirective implements OnInit {
  @Input('appChangeColor') ColorChange: string;
  constructor(private el: ElementRef, private renderer: Renderer2) {}
  ngOnInit(): void {
    if (this.ColorChange === 'true') {
      this.el.nativeElement.style.backgroundColor = 'green';
      this.el.nativeElement.style.color = 'white';
    } else {
      this.el.nativeElement.style.backgroundColor = 'red';
      this.el.nativeElement.style.color = 'white';
    }
  }
}
